# Study_FreeRTOS

Git for D.Grechushkin training

## Name
Studying and using FreeRTOS with DMA, timers, peripherals.

## Description
1) To implement a single device using the previous tasks, i.e. to collect already implemented tasks into one project using FreeRTOS:
	- LED flashing with the ability to change brightness by pressing a button;
	- Output of the debugging information to the PC via UART using DMA and a ring buffer.
2) It is necessary to create separate FreeRTOS tasks for each realized task.
3) Arrange data exchange between tasks using FreeRTOS mechanisms.
4) Launch WDT and organize work with it using FreeRTOS mechanisms.


1) Реализовать единое устройство, используя предыдущие задания, т.е. собрать уже реализованные задачи в один проект с использование FreeRTOS:
	- Мигание светодиода с возможностью изменения яркости по нажатию кнопки;
	- Вывод отладочной информации на ПК через UART с использованием DMA и кольцевого буфера.
2) Для каждого реализованного задания необходимо создать отдельные задачи FreeRTOS.
3) Организовать обмен данными между задачами с использованием механизмов FreeRTOS.
4) Запустить WDT и организовать работу с ним с использованием механизмов FreeRTOS.

## Authors and acknowledgment
Dmitriy Grchushkin

