/**
  ******************************************************************************
  * @file           : common.h
  * @brief          : Header for project files.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Created on: Oct 18, 2022
  * Author: dmitriy
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef INC_COMMON_H_
#define INC_COMMON_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stddef.h>

/* Private includes ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/**
  * @brief  Status Function Execution
  */
typedef enum
{
    OK_STATUS 		= 0U,
    INVALID_PARAMS 	= 1U,
    ERROR_STATUS 	= 2U,
	NOT_FINISHED	= 3U
} StatusFunctionExecution_t;

/**
  * @brief  Buffer TX/RX Type
  */
typedef enum
{
    RX_BUFFER = 0U,
    TX_BUFFER = 1U
} InterfaceBuffer_t;


/* Private includes ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/**
  * @brief  Status of interrupt flag
  */
typedef enum
{
    FLAG_DOWN = 0U,
    FLAG_UP = 1U
} FlagInterrupt_t;

/**
  * @brief State GPIO Led ON and Led OFF enumeration
  */
typedef enum
{
	LED_OFF = 0,
	LED_ON
} LedState_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/****************** Interrupt Flag : check FLAG_UP state **********************/
#define IS_INTERRUPT_FLAG_UP(FLAG)    ((FLAG) == FLAG_UP)

/****************** Led State : check LED_ON state ****************************/
#define IS_LED_ON(LED_STATE)    ((LED_STATE) == LED_ON)

/****************** Led State : check LED_OFF state ****************************/
#define IS_LED_OFF(LED_STATE)    ((LED_STATE) == LED_OFF)

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* Private defines -----------------------------------------------------------*/
//#define MX_DEBUGGING

#define LED_Pin GPIO_PIN_6
#define LED_GPIO_Port GPIOA
#define BUTTON_EXTI_IRQn EXTI0_IRQn

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/****************** Function State : check Function OK state ****************************/
#define IS_RESULT_OK(FUNCTION_STATE)    ((FUNCTION_STATE) == OK_STATUS)

/* Exported functions prototypes ---------------------------------------------*/
/* Private defines -----------------------------------------------------------*/

#endif /* INC_COMMON_H_ */
