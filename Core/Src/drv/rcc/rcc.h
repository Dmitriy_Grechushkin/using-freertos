/**
  ******************************************************************************
  * @file           : rcc.h
  * @brief          : Header for rcc.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 29, 2022
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef RCC_H_
#define RCC_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionExecution_t initializeSystemClock(void);
StatusFunctionExecution_t initializeGpioClock(void);

/* Private defines -----------------------------------------------------------*/

#endif /* RCC_H_ */
