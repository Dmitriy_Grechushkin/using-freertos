/**
  ******************************************************************************
  * @file           : watchdog.h
  * @brief          : Header for watchdog.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Nov 18, 2022
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRJ_DRV_WATCHDOG_H_
#define PRJ_DRV_WATCHDOG_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "stm32f4xx_hal.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
IWDG_HandleTypeDef* getWatchdogHandle(void);
StatusFunctionExecution_t initializeWatchdog(void);

/* Private defines -----------------------------------------------------------*/
/* External functions prototypes ---------------------------------------------*/

#endif /* PRJ_DRV_WATCHDOG_H_ */
