/**
  ******************************************************************************
  * @file    watchdog.c
  * @brief   watchdog Driver.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Nov 18, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "watchdog.h"
#if !defined (MX_DEBUGGING)
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define  WATCHDOG_PERIOD         	1023U  /*!< period of watchdog reload */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static IWDG_HandleTypeDef hiwdg;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
StatusFunctionExecution_t initializeWatchdog(void)
{
	hiwdg.Instance = IWDG;
	hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
	hiwdg.Init.Reload = WATCHDOG_PERIOD;
	if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
	{
		return ERROR_STATUS;
	}
	return OK_STATUS;
}
#else
extern IWDG_HandleTypeDef hiwdg;
#endif /* MX_DEBUGGING */
/**
  * @brief  Get Handle of timer PWM
  * @retval Pointer to Handle of timer PWM
  */
IWDG_HandleTypeDef* getWatchdogHandle(void)
{
	return &hiwdg;
}
