/**
  ******************************************************************************
  * @file    pwm.c
  * @brief   PWM Driver.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "pwm.h"
#if !defined (MX_DEBUGGING)
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static TIM_HandleTypeDef htim3;
static uint32_t Channel = TIM_CHANNEL_1;
static uint32_t FrequanceApb1 = 84000000;
static uint32_t CounterPeriod = 100;
static uint32_t FlickerFrequency = 250;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
StatusFunctionExecution_t initializeTimPwm(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = (FrequanceApb1 / (FlickerFrequency * CounterPeriod)) - 1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = CounterPeriod - 1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
	  return ERROR_STATUS;
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
	  return ERROR_STATUS;
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
	  return ERROR_STATUS;
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
	  return ERROR_STATUS;
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = DEFAULT_DUTY_CYCLE;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, Channel) != HAL_OK)
  {
	  return ERROR_STATUS;
  }
  HAL_TIM_MspPostInit(&htim3);

  if (HAL_TIM_PWM_Start(&htim3, Channel) != HAL_OK)
  {
	  return ERROR_STATUS;
  }

  return OK_STATUS;
}
#else
extern TIM_HandleTypeDef htim3;
static uint32_t Channel = TIM_CHANNEL_1;
static uint32_t FlickerFrequency = 250;
#endif /* MX_DEBUGGING */
/**
  * @brief  Get Handle of timer PWM
  * @retval Pointer to Handle of timer PWM
  */
TIM_HandleTypeDef* getTimPwmHandle(void)
{
	return &htim3;
}

/**
  * @brief  Get Handle of timer channel PWM
  * @retval Timer channel PWM
  */
uint32_t getTimPwmChannel(void)
{
	return Channel;
}

/**
  * @brief  Set pulse PWM
  * @param  PwmTimer: Handler of Pwm Timer
  * @param  Channel: Handler of Pwm channel
  * @param  Pulse: Pwm Pulse (duty cycle)
  * @retval Status update PWM pulse
  */
StatusFunctionExecution_t updatePwmPulse(TIM_HandleTypeDef* PwmTimer,
		                                 uint32_t Channel,
		                                 uint32_t Pulse)
{
	if ((Pulse > MAX_DUTY_CYCLE) || (PwmTimer == NULL))
	{
		return INVALID_PARAMS;
	}

	switch (Channel)
	{
	    case TIM_CHANNEL_1:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC1_INSTANCE(PwmTimer->Instance));

	      /* Return the capture 1 value */
	      PwmTimer->Instance->CCR1 = Pulse;

	      break;
	    }
	    case TIM_CHANNEL_2:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC2_INSTANCE(PwmTimer->Instance));

	      /* Return the capture 2 value */
	      PwmTimer->Instance->CCR2 = Pulse;

	      break;
	    }

	    case TIM_CHANNEL_3:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC3_INSTANCE(PwmTimer->Instance));

	      /* Return the capture 3 value */
	      PwmTimer->Instance->CCR3 = Pulse;

	      break;
	    }

	    case TIM_CHANNEL_4:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC4_INSTANCE(PwmTimer->Instance));

	      /* Return the capture 4 value */
	      PwmTimer->Instance->CCR4 = Pulse;

	      break;
	    }

	    default:
	    {
	    	return INVALID_PARAMS;
	    	break;
	    }
	}
    return OK_STATUS;
}

/**
  * @brief  Reset pulse PWM
  * @param  PwmTimer: Handler of Pwm Timer
  * @param  Channel: Handler of Pwm channel
  * @retval Status update PWM pulse
  */
StatusFunctionExecution_t resetPwmPulse(TIM_HandleTypeDef* PwmTimer,
                                        uint32_t Channel)
{
	if (PwmTimer == NULL)
	{
		return INVALID_PARAMS;
	}

	HAL_TIM_PWM_Stop(PwmTimer, Channel);

    return OK_STATUS;
}

/**
  * @brief  Restore pulse PWM
  * @param  PwmTimer: Handler of Pwm Timer
  * @param  Channel: Handler of Pwm channel
  * @retval Status update PWM pulse
  */
StatusFunctionExecution_t restorePwmPulse(TIM_HandleTypeDef* PwmTimer,
                                          uint32_t Channel)
{
	if (PwmTimer == NULL)
	{
		return INVALID_PARAMS;
	}

	HAL_TIM_PWM_Start(PwmTimer, Channel);

    return OK_STATUS;
}

/**
  * @brief Change Flicker Frequency PWM
  * @param  NewFlickerFrequency: New Flicker Frequency
  * @retval Status update Flicker Frequency
  */
StatusFunctionExecution_t changeFlickerFrequency(uint32_t NewFlickerFrequency)
{
	if (HAL_TIM_PWM_Stop(&htim3, Channel) != HAL_OK)
	{
	  return ERROR_STATUS;
	}

	FlickerFrequency = NewFlickerFrequency;
	initializeTimPwm();

    return OK_STATUS;
}


