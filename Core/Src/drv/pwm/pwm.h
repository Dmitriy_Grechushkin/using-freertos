/**
  ******************************************************************************
  * @file           : pwm.h
  * @brief          : Header for pwm.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRJ_DRV_PWM_H_
#define PRJ_DRV_PWM_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "stm32f4xx_hal.h"

/* Exported types ------------------------------------------------------------*/
/**
  * @brief PWM led initialize structure definition
  */
typedef struct
{

	volatile LedState_t State;       /*!< Specifies the PWM led to be configured.
                           	   	    This parameter can be any value LED_ON or LED_OFF */

	TIM_HandleTypeDef* PwmTimer;      /*!< Specifies the TIM Time Base Handle Structure definition */

	uint32_t Channel;      /*!< Specifies the Pull-up or Pull-Down activation for the selected pins.
                           	    This parameter can be a value of @ref TIM_Channel */

	volatile uint32_t Pulse;     /*!< Specifies the pulse for the PWM.
                           	 This parameter can be a value from 0 to MAX_PWM */

} PwmLed_t;

/* Exported constants --------------------------------------------------------*/
#define  MAX_DUTY_CYCLE         	100U  /*!< max value of the PWM pulse */
#define  DEFAULT_DUTY_CYCLE         100U  /*!< default value of the PWM pulse */

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
TIM_HandleTypeDef* getTimPwmHandle(void);
uint32_t getTimPwmChannel(void);
StatusFunctionExecution_t initializeTimPwm(void);
StatusFunctionExecution_t updatePwmPulse(TIM_HandleTypeDef*, uint32_t, uint32_t);
StatusFunctionExecution_t resetPwmPulse(TIM_HandleTypeDef*, uint32_t);
StatusFunctionExecution_t restorePwmPulse(TIM_HandleTypeDef*, uint32_t);
StatusFunctionExecution_t changeFlickerFrequency(uint32_t);

/* Private defines -----------------------------------------------------------*/
/* External functions prototypes ---------------------------------------------*/
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

#endif /* PRJ_DRV_PWM_H_ */
