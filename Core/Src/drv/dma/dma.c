/**
  ******************************************************************************
  * @file    dma.c
  * @brief   DMA Driver.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 29, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <dma.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief Enable DMA controller clock
  * @retval None
  */
StatusFunctionExecution_t initializeDma(void)
{
	/* DMA controller clock enable */
	__HAL_RCC_DMA2_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA2_Stream7_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

	return OK_STATUS;
}


