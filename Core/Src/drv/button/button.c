/**
  ******************************************************************************
  * @file    button.c
  * @brief   Button Driver.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "button.h"
#include "stm32f4xx_hal.h"

/* Private define ------------------------------------------------------------*/
#define MAX_CHECK_NUMBER    8U
#define MAX_NUMBER_CONFIRMATIONS    5U

#define BUTTON_Pin GPIO_PIN_0
#define BUTTON_GPIO_Port GPIOB
/* Private variables ---------------------------------------------------------*/

/* Counter of the number of button status checks */
static uint32_t CounterButtonStatusCheck = 0;

/* Counter of the success number of checks of the button state */
static uint32_t CounterSuccessCheck = 0;

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
StatusFunctionExecution_t initializeButton(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /*Configure GPIO pin : BUTTON_Pin */
  GPIO_InitStruct.Pin = BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(BUTTON_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(BUTTON_EXTI_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(BUTTON_EXTI_IRQn);

  return OK_STATUS;

}
/**
  * @brief Initialization initial button state
  * @param None
  * @retval None
  */
StatusButton_t initializeButtonState(void)
{
	if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_SET)
	{
		return CLICK;
	}
	else
	{
		return IDLE;
	}
}

/**
  * @brief Check changing button state
  * @param None
  * @retval Status of checking
  */
StatusStateCheck_t checkButtonStateChange(StatusButton_t LastState)
{
	CounterButtonStatusCheck++;

	if (LastState == IDLE)
	{
		if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_SET)
		{
			CounterSuccessCheck++;
		}
	}
	else if (LastState == CLICK)
	{

		if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_RESET)
		{
			CounterSuccessCheck++;
		}

	}
	if (CounterSuccessCheck > MAX_NUMBER_CONFIRMATIONS)
	{
		CounterSuccessCheck = 0;
		CounterButtonStatusCheck = 0;

		return CHANGED;
	}
	if (CounterButtonStatusCheck > MAX_CHECK_NUMBER)
	{

		CounterSuccessCheck = 0;
		CounterButtonStatusCheck = 0;

		return NO_CHANGED;
	}

	return IN_PROCESS;
}



