/**
  ******************************************************************************
  * @file           : button.h
  * @brief          : Header for button.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRJ_DRV_BUTTON_H_
#define PRJ_DRV_BUTTON_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"

/* Exported types ------------------------------------------------------------*/

/**
  * @brief  Button status
  */
typedef enum
{
    IDLE = 0U,
    PRESSED = 1U,
	CLICK = 2U,
	RELEASED = 3U
} StatusButton_t;

/**
  * @brief  Status of Checking Button State
  */
typedef enum
{
    NO_CHANGED = 0U,
	CHANGED = 1U,
	IN_PROCESS = 2U
} StatusStateCheck_t;

/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionExecution_t initializeButton(void);
StatusButton_t initializeButtonState(void);

StatusStateCheck_t checkButtonStateChange(StatusButton_t);
/* Private defines -----------------------------------------------------------*/


#endif /* PRJ_DRV_BUTTON_H_ */
