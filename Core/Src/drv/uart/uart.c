/**
  ******************************************************************************
  * @file    uart.c
  * @brief   UART Module.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 22, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "uart.h"
#include "common.h"

#if !defined (MX_DEBUGGING)
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define BAUD_RATE                     115200U
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval Status of initialization
  */
StatusUart_t UART_initialize(void)
{
	huart1.Instance = USART1;
	huart1.Init.BaudRate = BAUD_RATE;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart1) != HAL_OK)
	{
		return UART_STATE_ERROR;
	}
	return UART_STATE_OK;
}

/**
  * @brief Get of UART Handle
  * @param None
  * @retval Pointer to UART Handle
  */
UART_HandleTypeDef* getUartHandle(void)
{
	return &huart1;
}
#else
extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_tx;
#endif /* MX_DEBUGGING */

/**
  * @brief Data transfer via UART with DMA
  * @param Type: type of Buffer Structure
  * @param Length: buffer length
  * @retval Status of transfer
  */
StatusUart_t UART_transmitDmaRing(uint8_t* TxArray, uint16_t Length)
{
	HAL_StatusTypeDef UartStatus;
	UartStatus = HAL_UART_Transmit_DMA(&huart1, (const uint8_t*) TxArray, Length);
	if (UartStatus == HAL_BUSY)
	{
		return UART_STATE_BUSY;
	}
	else if (UartStatus == HAL_OK)
	{
		return UART_STATE_OK;
	}
	else
	{
		return UART_STATE_ERROR;
	}
}

/**
  * @brief Data transfer via UART
  * @param Type: type of Buffer Structure
  * @param Length: buffer length
  * @retval Status of transfer
  */
StatusUart_t UART_transmit(uint8_t* TxArray, uint16_t Length)
{
	HAL_StatusTypeDef UartStatus;
	UartStatus = HAL_UART_Transmit(&huart1, (const uint8_t*) TxArray, Length, 100);
	if (UartStatus == HAL_BUSY)
	{
		return UART_STATE_BUSY;
	}
	else if (UartStatus == HAL_OK)
	{
		return UART_STATE_OK;
	}
	else
	{
		return UART_STATE_ERROR;
	}
}

/**
  * @brief Data transfer via UART (CMSIS)
  * @param Type: type of Buffer Structure
  * @param Length: buffer length
  * @retval Status of transfer
  */
StatusUart_t myUART_transmit(uint8_t* TxArray, uint16_t Length)
{
	for(int i = 0; i < Length; i++)
	{
		while(!	(huart1.Instance->SR & USART_SR_TC));
		huart1.Instance->DR = *(TxArray + i);
	}
	return UART_STATE_OK;
}




