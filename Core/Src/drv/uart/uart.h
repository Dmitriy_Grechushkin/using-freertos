/**
  ******************************************************************************
  * @file           : uart.h
  * @brief          : Header for uart.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 22, 2022
  *
  ******************************************************************************
  */
#ifndef SRC_DRV_UART_UART_H_
#define SRC_DRV_UART_UART_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "stm32f4xx_hal.h"

/* Private defines -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/**
  * @brief  Result value update
  */
typedef enum
{
	UART_STATE_OK 		= 0U,
	UART_STATE_BUSY 	= 1U,
	UART_STATE_ERROR 	= 2U
} StatusUart_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusUart_t UART_initialize(void);
StatusUart_t UART_transmitDmaRing(uint8_t*, uint16_t);
StatusUart_t UART_transmit(uint8_t*, uint16_t);
StatusUart_t myUART_transmit(uint8_t*, uint16_t);
UART_HandleTypeDef* getUartHandle(void);

#endif /* SRC_DRV_UART_UART_H_ */
