/**
  ******************************************************************************
  * @file           : led.h
  * @brief          : Header for led.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRJ_DRV_LED_H_
#define PRJ_DRV_LED_H_

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "pwm.h"

/* Exported types ------------------------------------------------------------*/

/**
  * @brief  Timer blinking period
  */
typedef enum {
    PERIOD_200_MS = 200U,
    PERIOD_100_MS = 100U
} Tim2Period;


/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionExecution_t initializePwmLed(PwmLed_t*, TIM_HandleTypeDef*, uint32_t, uint16_t);
StatusFunctionExecution_t togglePwmLed(PwmLed_t*);
StatusFunctionExecution_t increasePwmLedPulse(PwmLed_t*);
StatusFunctionExecution_t turnOffPwmLed(PwmLed_t*);
StatusFunctionExecution_t turnOnPwmLed(PwmLed_t*);

#endif /* PRJ_DRV_LED_H_ */
