/**
  ******************************************************************************
  * @file    led.c
  * @brief   Led Driver.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "led.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define  DUTY_CYCLE_SHIFT         10U    /*!< duty cycle shift */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief Initialization Function of PWM Led
  * This function configures timer that change led state
  * @param Led: PwmLed object
  * @param htim: timer handle
  * @param Channel: channel of timer
  * @param Pulse: initial PWM pulse
  * @retval None
  */
StatusFunctionExecution_t initializePwmLed(PwmLed_t* Led, TIM_HandleTypeDef* htim,
								   uint32_t Channel, uint16_t Pulse)
{
	if ((Led == NULL) || (htim == NULL) || (Pulse > MAX_DUTY_CYCLE) ||
		(!IS_TIM_CCX_INSTANCE(htim->Instance, Channel)))
	{
		return INVALID_PARAMS;
	}

	Led->State = LED_ON;
	Led->PwmTimer = htim;
	Led->Channel = Channel;
	Led->Pulse = Pulse;

	return OK_STATUS;
}

/**
  * @brief Toggle PWM Led
  * @param Led: PwmLed object
  * @retval None
  */
StatusFunctionExecution_t togglePwmLed(PwmLed_t* Led)
{
	if ((Led == NULL) || (Led->PwmTimer == NULL))
	{
		return INVALID_PARAMS;
	}

	if (Led->State == LED_ON) {
		Led->State = LED_OFF;

		/* Reset the PWM Pulse */
		if (turnOffPwmLed(Led) != OK_STATUS)
		{
			return ERROR_STATUS;
		}
	}
	else if (Led->State == LED_OFF)
	{
		Led->State = LED_ON;

		/* Restore the PWM Pulse */
		if (turnOnPwmLed(Led) != OK_STATUS) {
			return ERROR_STATUS;
		}
	}
	return OK_STATUS;
}

/**
  * @brief Increase PWM Led Pulse
  * @param Led: PwmLed object
  * @retval Status of updating
  */
StatusFunctionExecution_t increasePwmLedPulse(PwmLed_t* Led)
{
	if ((Led == NULL) || (Led->PwmTimer == NULL))
	{
		return INVALID_PARAMS;
	}

	Led->Pulse += DUTY_CYCLE_SHIFT;
	if (Led->Pulse > MAX_DUTY_CYCLE)
	{
		Led->Pulse = 0;
	}
	if (updatePwmPulse(Led->PwmTimer, Led->Channel, Led->Pulse) != OK_STATUS)
	{
		return ERROR_STATUS;
	}
	return OK_STATUS;
}

/**
  * @brief  Turn off PWM Led
  * @param  Led: PwmLed object
  * @retval Status update PWM pulse
  */
StatusFunctionExecution_t turnOffPwmLed(PwmLed_t* Led)
{
	if (Led == NULL)
	{
		return INVALID_PARAMS;
	}
	if (resetPwmPulse(Led->PwmTimer, Led->Channel) != OK_STATUS)
	{
		return ERROR_STATUS;
	}
    return OK_STATUS;
}

/**
  * @brief  Turn on PWM Led
  * @param  Led: PwmLed object
  * @retval Status update PWM pulse
  */
StatusFunctionExecution_t turnOnPwmLed(PwmLed_t* Led)
{
	if (Led == NULL)
	{
		return INVALID_PARAMS;
	}
	if (restorePwmPulse(Led->PwmTimer, Led->Channel) != OK_STATUS)
	{
		return ERROR_STATUS;
	}
	return OK_STATUS;
}



