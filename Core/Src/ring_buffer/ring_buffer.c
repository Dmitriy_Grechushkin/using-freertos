/**
  ******************************************************************************
  * @file    ring_buffer.c
  * @brief   Ring Buffer Module.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 18, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "ring_buffer.h"

/* Private define ------------------------------------------------------------*/
#define UART_RX_BUFFER_SIZE    256	  /*!< size of RX Ring Buffer */
#define UART_TX_BUFFER_SIZE    256	  /*!< size of TX Ring Buffer */

/* Private variables ---------------------------------------------------------*/
static uint8_t RxArray[UART_RX_BUFFER_SIZE];
static uint8_t TxArray[UART_TX_BUFFER_SIZE];

static RingBuffer_t RxRingBuffer;
static RingBuffer_t TxRingBuffer;

/**
  * @brief Get of TX Ring Buffer Structure
  * @param None
  * @retval Pointer to TX RB Structure
  */
RingBuffer_t* getTxRingBuffer(void)
{
	return &TxRingBuffer;
}

/**
  * @brief Get of RX Ring Buffer Structure
  * @param None
  * @retval Pointer to RX RB Structure
  */
RingBuffer_t* getRxRingBuffer(void)
{
	return &RxRingBuffer;
}

/**
  * @brief Initialization Function of Ring Buffer Structure
  * @param Type: type of buffer
  * @retval Status RB initialization
  */
StatusRingBuffer_t initializeRingBuffer(InterfaceBuffer_t Type)
{

	if (Type == RX_BUFFER)
	{
		RxRingBuffer.Length = UART_RX_BUFFER_SIZE;
		RxRingBuffer.Head = RxArray;
		RxRingBuffer.InputItem = RxArray;
		RxRingBuffer.NumberOfItems = 0;
	}
	else if (Type == TX_BUFFER)
	{
		TxRingBuffer.Length = UART_TX_BUFFER_SIZE;
		TxRingBuffer.Head = TxArray;
		TxRingBuffer.InputItem = TxArray;
		TxRingBuffer.NumberOfItems = 0;
	}
	else
	{
		return RB_ERROR;
	}

	return RB_OK;
}

/**
  * @brief Add new char items to Ring Buffer
  * @param RingBuffer: pointer to ring buffer structure
  * @param Item: pointer to new char items
  * @param Size: size of new char items
  * @retval Status function
  */
StatusRingBuffer_t RB_putNewItems(RingBuffer_t* RingBuffer, const char* Item, uint8_t Size)
{

    if ((RingBuffer == NULL) || (Item == NULL))
    {
    	return RB_INVALID_PARAMS;
    }

    for (uint8_t i = 0; i < Size; i++)
    {
    	/* add new item */
        *(RingBuffer->InputItem) = *(Item + i);

        /* pointer shift */
        RingBuffer->InputItem++;

        /* check buffer overrun */
        if (RingBuffer->InputItem >= (RingBuffer->Head + RingBuffer->Length))
        {
        	RingBuffer->InputItem = RingBuffer->Head;
        }

        /* check overwriting of old items */
        if (RingBuffer->NumberOfItems < RingBuffer->Length)
        {
        	RingBuffer->NumberOfItems++;
        }
    }

    return RB_OK;
}

/**
  * @brief Get Number Of RingBuffer Items
  * @param RingBuffer: pointer to ring buffer structure
  * @retval Number Of RingBuffer Items
  */
uint16_t RB_getNumberOfItems(RingBuffer_t* RingBuffer)
{
	return RingBuffer->NumberOfItems;
}

/**
  * @brief Get Pointer to Output Item
  * @param RingBuffer: pointer to ring buffer structure
  * @retval Output Item Pointer
  */
uint8_t* RB_getOutputItemPointer(RingBuffer_t* RingBuffer)
{
	uint8_t* OutputItem = (uint8_t*) RingBuffer->InputItem - (uint16_t) RingBuffer->NumberOfItems;
	if (OutputItem < RingBuffer->Head)
		OutputItem += RingBuffer->Length;
	return OutputItem;
}

/**
  * @brief Get the number of elements to the end of the buffer array
  * @param RingBuffer: pointer to ring buffer structure
  * @retval Number Of RingBuffer Items Before Array End
  */
uint16_t RB_getNumberOfItemsBeforeEnd(RingBuffer_t* RingBuffer)
{
	uint8_t* OutputItem = RB_getOutputItemPointer(RingBuffer);
	if (RingBuffer->InputItem > OutputItem)
	{
		return (RingBuffer->NumberOfItems);
	}
	else
	{
		return ((RingBuffer->Head + RingBuffer->Length) - OutputItem);
	}
}

/**
  * @brief Decrease Number of Items of Ring Buffer
  * @param RingBuffer: pointer to ring buffer structure
  * @param Number: Number of items
  * @retval Status function
  */
StatusRingBuffer_t RB_decreaseNumberOfItems(RingBuffer_t* RingBuffer, uint16_t Number)
{
	if ((RingBuffer == NULL) || (Number > RingBuffer->NumberOfItems))
	{
		return RB_INVALID_PARAMS;
	}

	RingBuffer->NumberOfItems -= Number;

	return RB_OK;
}



