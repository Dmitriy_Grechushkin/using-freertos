/**
  ******************************************************************************
  * @file           : types_converting.h
  * @brief          : Header for types_converting.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 20, 2022
  *
  ******************************************************************************
  */
#ifndef SRC_DATA_LOGGING_TYPES_CONVERTING_H_
#define SRC_DATA_LOGGING_TYPES_CONVERTING_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"

/* Private defines -----------------------------------------------------------*/
#define MAX_STRING_SIZE	  10

/* Exported types ------------------------------------------------------------*/
/**
  * @brief  Structure of integer in string format
  */
typedef struct
{
	uint8_t FirstNotNullPosition;
	char String[MAX_STRING_SIZE];
} IntAsString_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
IntAsString_t* convertIntToString(uint32_t);

#endif /* SRC_DATA_LOGGING_TYPES_CONVERTING_H_ */
