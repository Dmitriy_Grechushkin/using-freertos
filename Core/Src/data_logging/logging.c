/**
  ******************************************************************************
  * @file    logging.c
  * @brief   Data logging Module.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 20, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "logging.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TWO_CHARS_SIZE	  2
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static char RegularBuffer[REGULAR_BUFFER_SIZE];

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief Initialization Function of Common Buffer Structure
  * @param Type: type of Buffer Structure
  * @param CommonBuffer: pointer to common buffer structure
  * @retval Status common buffer initialization
  */
StatusFunctionExecution_t initializeCommonBuffer(BufferType_t Type,
												 CommonBuffer_t* CommonBuffer)
{
	if (Type == REGULAR_BUFFER)
	{
		CommonBuffer->RegularBuffer = &RegularBuffer;
	}
	else if (Type == RING_BUFFER_TX)
	{
		if (initializeRingBuffer(TX_BUFFER) == RB_OK)
		{
			CommonBuffer->RingBuffer = getTxRingBuffer();
		}
	}
	else if (Type == RING_BUFFER_RX)
	{
		if (initializeRingBuffer(RX_BUFFER) == RB_OK)
		{
			CommonBuffer->RingBuffer = getRxRingBuffer();
		}
	}
	else
	{
		return ERROR_STATUS;
	}

	CommonBuffer->BufferType = Type;

	return OK_STATUS;
}

/**
  * @brief Addition log message to buffer
  * @param Buffer: pointer to common buffer structure
  * @param format: log message array
  * @param Value: Value log
  * @retval Status execution
  */
StatusFunctionExecution_t addLogMessage(CommonBuffer_t* Buffer,
										const char* format, uint32_t Value)
{
	if (Buffer == NULL)
	{
		return INVALID_PARAMS;
	}

#if defined(SPRINTF)
	char tempBuffer[64];
	sprintf(tempBuffer, format, Value);
	RB_putNewItems(Buffer->RingBuffer, tempBuffer, strlen(tempBuffer));
#else
	if (Buffer->BufferType == RING_BUFFER_TX)
	{
		uint32_t SizeString = 0;
		while (*(format + SizeString) != '\0')
		{
			SizeString++;
		}
		StatusFunctionExecution_t FunctionStatus;
		FunctionStatus = RB_putNewItems(Buffer->RingBuffer, format, SizeString);
		if (FunctionStatus != OK_STATUS)
		{
			return ERROR_STATUS;
		}

		IntAsString_t* TempStringStructure = convertIntToString(Value);
		FunctionStatus = RB_putNewItems(Buffer->RingBuffer,
					   	   	   	   	    TempStringStructure->String +
										TempStringStructure->FirstNotNullPosition,
										MAX_STRING_SIZE -
										TempStringStructure->FirstNotNullPosition);
		if (FunctionStatus != OK_STATUS)
		{
			return ERROR_STATUS;
		}

		/* transfer to new line */
		FunctionStatus = RB_putNewItems(Buffer->RingBuffer, "\r\n", TWO_CHARS_SIZE);
		if (FunctionStatus != OK_STATUS)
		{
			return ERROR_STATUS;
		}
	}
#endif

	return OK_STATUS;
}

/**
  * @brief Addition log message to buffer without value
  * @param Buffer: pointer to common buffer structure
  * @param format: log message array
  * @retval Status execution
  */
StatusFunctionExecution_t addLogMessageWithoutValue(CommonBuffer_t* Buffer,
													const char* format)
{
	if (Buffer == NULL)
	{
		return INVALID_PARAMS;
	}

	if (Buffer->BufferType == RING_BUFFER_TX)
	{
		uint32_t SizeString = 0;
		while (*(format + SizeString) != '\0')
		{
			SizeString++;
		}
		StatusFunctionExecution_t FunctionStatus;
		FunctionStatus = RB_putNewItems(Buffer->RingBuffer, format, SizeString);
		if (FunctionStatus != OK_STATUS)
		{
			return ERROR_STATUS;
		}

		/* transfer to new line */
		FunctionStatus = RB_putNewItems(Buffer->RingBuffer, "\r\n", TWO_CHARS_SIZE);
		if (FunctionStatus != OK_STATUS)
		{
			return ERROR_STATUS;
		}
	}

	return OK_STATUS;
}


/**
  * @brief Print logged message
  * @param Buffer: pointer to common buffer structure
  * @retval Status execution
  */
StatusFunctionExecution_t print(CommonBuffer_t* Buffer)
{
	if (Buffer->BufferType == RING_BUFFER_TX)
	{
		uint16_t NumberOfItems = RB_getNumberOfItemsBeforeEnd(Buffer->RingBuffer);
		uint8_t* RingBufferOutputItem = RB_getOutputItemPointer(Buffer->RingBuffer);

		StatusUart_t UartStatus;
		UartStatus = UART_transmitDmaRing(RingBufferOutputItem, NumberOfItems);
		if (UartStatus == UART_STATE_OK)
		{
			if (RB_decreaseNumberOfItems(Buffer->RingBuffer, NumberOfItems) != RB_OK)
			{
				return ERROR_STATUS;
			}
		}
		else if (UartStatus == UART_STATE_ERROR)
		{
			return ERROR_STATUS;
		}
		else
		{
			return NOT_FINISHED;
		}
	}
	return OK_STATUS;
}

