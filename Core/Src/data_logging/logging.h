/**
  ******************************************************************************
  * @file           : logging.h
  * @brief          : Header for logging.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 18, 2022
  *
  ******************************************************************************
  */

#ifndef SRC_DATA_LOGGING_LOGGING_H_
#define SRC_DATA_LOGGING_LOGGING_H_

/* Includes ------------------------------------------------------------------*/
#include "ring_buffer.h"
#include "uart.h"

#if defined(SPRINTF)
#include <stdio.h>
#else
#include "types_converting.h"
#endif
/* Private defines -----------------------------------------------------------*/
#define REGULAR_BUFFER_SIZE	  256

/* Exported types ------------------------------------------------------------*/

/**
  * @brief  Buffer Type
  */
typedef enum
{
    REGULAR_BUFFER = 0U,
    RING_BUFFER_RX = 1U,
	RING_BUFFER_TX = 2U
} BufferType_t;

/**
  * @brief  Common Buffer Structure - wrap for buffer type
  */
typedef struct
{
	RingBuffer_t* RingBuffer;
	char (*RegularBuffer)[REGULAR_BUFFER_SIZE];
	BufferType_t BufferType;

} CommonBuffer_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionExecution_t initializeCommonBuffer(BufferType_t, CommonBuffer_t*);
StatusFunctionExecution_t addLogMessage(CommonBuffer_t*, const char*, uint32_t);
StatusFunctionExecution_t addLogMessageWithoutValue(CommonBuffer_t*, const char*);
StatusFunctionExecution_t print(CommonBuffer_t*);

#endif /* SRC_DATA_LOGGING_LOGGING_H_ */
