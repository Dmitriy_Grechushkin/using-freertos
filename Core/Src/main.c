/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
#if defined (MX_DEBUGGING)
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "semphr.h"
#include "os_wrapped.h"
#include "led.h"
#include "button.h"
#else
#include "main.h"
#include "os_tasks.h"
#include "dma.h"
#include "rcc.h"
#include "watchdog.h"
#endif /* MX_DEBUGGING */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#if defined (MX_DEBUGGING)
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
IWDG_HandleTypeDef hiwdg;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_tx;

/* Definitions for debounceTask */
osThreadId_t debounceTaskHandle;
const osThreadAttr_t debounceTask_attributes = {
  .name = "debounceTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityAboveNormal,
};
/* Definitions for blinkingTask */
osThreadId_t blinkingTaskHandle;
const osThreadAttr_t blinkingTask_attributes = {
  .name = "blinkingTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for debuggingTask */
osThreadId_t debuggingTaskHandle;
const osThreadAttr_t debuggingTask_attributes = {
  .name = "debuggingTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for watchdogTask */
osThreadId_t watchdogTaskHandle;
const osThreadAttr_t watchdogTask_attributes = {
  .name = "watchdogTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityHigh7,
};
/* Definitions for Uart2Mutex */
osMutexId_t Uart2MutexHandle;
const osMutexAttr_t Uart2Mutex_attributes = {
  .name = "Uart2Mutex"
};
/* Definitions for buttonExtiBinarySem */
osSemaphoreId_t buttonExtiBinarySemHandle;
const osSemaphoreAttr_t buttonExtiBinarySem_attributes = {
  .name = "buttonExtiBinarySem"
};
/* Definitions for debugTaskBinarySem */
osSemaphoreId_t debugTaskBinarySemHandle;
const osSemaphoreAttr_t debugTaskBinarySem_attributes = {
  .name = "debugTaskBinarySem"
};
/* USER CODE BEGIN PV */
#endif /* MX_DEBUGGING */

PwmLed_t PmwLed;
CommonBuffer_t TxRingBuffer;

#if defined (MX_DEBUGGING)
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM3_Init(void);
static void MX_IWDG_Init(void);
void StartDebounceTask(void *argument);
void StartBinkingTask(void *argument);
void StartDebuggingTask(void *argument);
void StartWatchdogTask(void *argument);

/* USER CODE BEGIN PFP */
#endif /* MX_DEBUGGING */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
#if defined (MX_DEBUGGING)
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_IWDG_Init();
  /* USER CODE BEGIN 2 */
#else
  /* Configure the system clock */
  initializeSystemClock();

  /* Initialize all configured peripherals */
  initializeGpioClock();
  initializeButton();
  initializeDma();
  UART_initialize();
  initializeTimPwm();
  initializeWatchdog();

  if (initializePwmLed(&PmwLed, getTimPwmHandle(), getTimPwmChannel(), MAX_DUTY_CYCLE) != OK_STATUS) {
	  Error_Handler();
  }
  if (initializeCommonBuffer(RING_BUFFER_TX, &TxRingBuffer) != OK_STATUS)
  {
	  Error_Handler();
  }
#endif /* MX_DEBUGGING */
#if defined (MX_DEBUGGING)
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();
  /* Create the mutex(es) */
  /* creation of Uart2Mutex */
  Uart2MutexHandle = osMutexNew(&Uart2Mutex_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
#else
  /* Init scheduler */
  osKernelInitialize();
  /* Create the mutex(es) */
  createMutexes();
#endif /* MX_DEBUGGING */
  /* add mutexes, ... */
#if defined (MX_DEBUGGING)
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of buttonExtiBinarySem */
  buttonExtiBinarySemHandle = osSemaphoreNew(1, 1, &buttonExtiBinarySem_attributes);

  /* creation of debugTaskBinarySem */
  debugTaskBinarySemHandle = osSemaphoreNew(1, 1, &debugTaskBinarySem_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
#else
  createSemaphores();
#endif /* MX_DEBUGGING */
  /* add semaphores, ... */
#if defined (MX_DEBUGGING)
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of debounceTask */
  debounceTaskHandle = osThreadNew(StartDebounceTask, NULL, &debounceTask_attributes);

  /* creation of blinkingTask */
  blinkingTaskHandle = osThreadNew(StartBinkingTask, NULL, &blinkingTask_attributes);

  /* creation of debuggingTask */
  debuggingTaskHandle = osThreadNew(StartDebuggingTask, NULL, &debuggingTask_attributes);

  /* creation of watchdogTask */
  watchdogTaskHandle = osThreadNew(StartWatchdogTask, NULL, &watchdogTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
#else
  createThreads();
#endif /* MX_DEBUGGING */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}
/* USER CODE BEGIN DEBUG */
#if defined (MX_DEBUGGING)
/* USER CODE END DEBUG */
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
  hiwdg.Init.Reload = 1023;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = (84000000 / (250 * 100)) - 1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 100 - 1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : BUTTON_Pin */
  GPIO_InitStruct.Pin = BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(BUTTON_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDebounceTask */
/**
  * @brief  Function implementing the debounceTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDebounceTask */
void StartDebounceTask(void *argument)
{
  /* USER CODE BEGIN 5 */
	StatusButton_t ButtonState;
	ButtonState = initializeButtonState();
	xSemaphoreTake(buttonExtiBinarySemHandle, portMAX_DELAY);
    /* Infinite loop */
    for(;;)
    {
    	xSemaphoreTake(buttonExtiBinarySemHandle, portMAX_DELAY);
    	addLogMessageWithoutValueWrapped(&TxRingBuffer, "DebounceTask");
    	while (1)
    	{
    		StatusStateCheck_t TempStatusChecking = checkButtonStateChange(ButtonState);

			if (TempStatusChecking == CHANGED)
			{
				if (ButtonState == IDLE)
				{
				    ButtonState = CLICK;

				    increasePwmLedPulse(&PmwLed);
			    }
			    else if (ButtonState == CLICK)
			    {
				    ButtonState = IDLE;
			    }

			    break;
			}
			else if (TempStatusChecking == NO_CHANGED)
			{
				break;
			}
    		osDelay(10);
    	}
    	/* Enable button interruptions */
		HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartBinkingTask */
/**
* @brief Function implementing the blinkingTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartBinkingTask */
void StartBinkingTask(void *argument)
{
  /* USER CODE BEGIN StartBinkingTask */
  /* Infinite loop */
  for(;;)
  {
	  togglePwmLed(&PmwLed);
	  if (IS_LED_ON(PmwLed.State))
	  {
		  /* Update the timer for 200 ms */
		  osDelay(200);
	  }
	  else if (IS_LED_OFF(PmwLed.State))
	  {
		  /* Update the timer for 100 ms */
		  osDelay(100);
	  }
  }
  /* USER CODE END StartBinkingTask */
}

/* USER CODE BEGIN Header_StartDebuggingTask */
/**
* @brief Function implementing the debuggingTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDebuggingTask */
void StartDebuggingTask(void *argument)
{
  /* USER CODE BEGIN StartDebuggingTask */
	StatusFunctionExecution_t PrintStatus;
	xSemaphoreTake(debugTaskBinarySemHandle, portMAX_DELAY);
  /* Infinite loop */
  for(;;)
  {
	  xSemaphoreTake(debugTaskBinarySemHandle, portMAX_DELAY);
	  PrintStatus = printWrapped(&TxRingBuffer);
	  if (PrintStatus != OK_STATUS)
	  {
		  if (PrintStatus == NOT_FINISHED)
		  {
			  addLogMessageWithoutValueWrapped(&TxRingBuffer, "DMA is busy");
		  }
		  else
		  {
			  vTaskSuspend(debuggingTaskHandle);
		  }
	  }

	  /* if there is still data in the ring buffer, give the semaphore again */
	  if (RB_getNumberOfItems(TxRingBuffer.RingBuffer))
	  {
		  xSemaphoreGive(debugTaskBinarySemHandle);
	  }
	  osDelay(5);
  }
  /* USER CODE END StartDebuggingTask */
}

/* USER CODE BEGIN Header_StartWatchdogTask */
/**
* @brief Function implementing the watchdogTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartWatchdogTask */
void StartWatchdogTask(void *argument)
{
  /* USER CODE BEGIN StartWatchdogTask */
  /* Infinite loop */
  for(;;)
  {
	  osDelay(500);
	  HAL_IWDG_Refresh(&hiwdg);
  }
  /* USER CODE END StartWatchdogTask */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM5 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM5) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/* USER CODE BEGIN DEBUG2 */
#endif /* MX_DEBUGGING */
/* USER CODE END DEBUG2 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
