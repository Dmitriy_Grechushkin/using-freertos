/**
  ******************************************************************************
  * @file           : os_tasks.h
  * @brief          : Header for os_tasks.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Nov 17, 2022
  *
  ******************************************************************************
  */

#ifndef SRC_OS_TASKS_H_
#define SRC_OS_TASKS_H_

/* Includes ------------------------------------------------------------------*/
#include "os_wrapped.h"

#include "button.h"
#include "led.h"
#include "watchdog.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionExecution_t createMutexes(void);
StatusFunctionExecution_t createSemaphores(void);
StatusFunctionExecution_t createThreads(void);

/* Private defines -----------------------------------------------------------*/


#endif /* SRC_OS_TASKS_H_ */
