/**
  ******************************************************************************
  * @file    os_tasks.c
  * @brief   My tasks of FreeRTOS.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Nov 17, 2022
  *
  ******************************************************************************
  */
#include "os_tasks.h"
#if !defined (MX_DEBUGGING)
/* Includes ------------------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define INITIAL_SEMAPHORE_COUNT		0
#define MAX_SEMAPHORE_COUNT			1

#define DEBOUCE_STACK_WORDS	  		128U
#define BLINKING_STACK_WORDS	  	128U
#define DEBUGGING_STACK_WORDS	  	256U
#define WATCHDOG_STACK_WORDS	  	128U

#define BYTES_IN_WORD	  			4U

#define DELAY_WATCHDOG_TASK			500U
#define DELAY_DEBUGGING_TASK		5U

/* Private variables ---------------------------------------------------------*/
/* Definitions for debounceTask */
osThreadId_t debounceTaskHandle;
const osThreadAttr_t debounceTask_attributes = {
  .name = "debounceTask",
  .stack_size = DEBOUCE_STACK_WORDS * BYTES_IN_WORD,
  .priority = (osPriority_t) osPriorityAboveNormal,
};
/* Definitions for blinkingTask */
osThreadId_t blinkingTaskHandle;
const osThreadAttr_t blinkingTask_attributes = {
  .name = "blinkingTask",
  .stack_size = BLINKING_STACK_WORDS * BYTES_IN_WORD,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for debuggingTask */
osThreadId_t debuggingTaskHandle;
const osThreadAttr_t debuggingTask_attributes = {
  .name = "debuggingTask",
  .stack_size = DEBUGGING_STACK_WORDS * BYTES_IN_WORD,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for watchdogTask */
osThreadId_t watchdogTaskHandle;
const osThreadAttr_t watchdogTask_attributes = {
  .name = "watchdogTask",
  .stack_size = WATCHDOG_STACK_WORDS * BYTES_IN_WORD,
  .priority = (osPriority_t) osPriorityHigh7,
};

/* Definitions for Uart2Mutex */
osMutexId_t Uart2MutexHandle;
const osMutexAttr_t Uart2Mutex_attributes = {
  .name = "Uart2Mutex"
};

/* Definitions for buttonExtiBinarySem */
osSemaphoreId_t buttonExtiBinarySemHandle;
const osSemaphoreAttr_t buttonExtiBinarySem_attributes = {
  .name = "buttonExtiBinarySem"
};
/* Definitions for debugTaskBinarySem */
osSemaphoreId_t debugTaskBinarySemHandle;
const osSemaphoreAttr_t debugTaskBinarySem_attributes = {
  .name = "debugTaskBinarySem"
};

/* Private function prototypes -----------------------------------------------*/
void StartDebounceTask(void *argument);
void StartBinkingTask(void *argument);
void StartDebuggingTask(void *argument);
void StartWatchdogTask(void *argument);

/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
extern PwmLed_t PmwLed;
extern CommonBuffer_t TxRingBuffer;

/* External functions --------------------------------------------------------*/
/**
  * @brief  Create the mutex(es)
  * @retval Status creating
  */
StatusFunctionExecution_t createMutexes(void)
{

	/* creation of Uart2Mutex */
	Uart2MutexHandle = osMutexNew(&Uart2Mutex_attributes);

	if (Uart2MutexHandle == NULL)
	{
		return ERROR_STATUS;
	}

	return OK_STATUS;
}

/**
  * @brief  Create the semaphores
  * @retval Status creating
  */
StatusFunctionExecution_t createSemaphores(void)
{
	/* creation of buttonExtiBinarySem */
	buttonExtiBinarySemHandle = osSemaphoreNew(MAX_SEMAPHORE_COUNT, INITIAL_SEMAPHORE_COUNT,
			                                   &buttonExtiBinarySem_attributes);

	/* creation of debugTaskBinarySem */
	debugTaskBinarySemHandle = osSemaphoreNew(MAX_SEMAPHORE_COUNT, INITIAL_SEMAPHORE_COUNT,
			                                  &debugTaskBinarySem_attributes);

	if ((buttonExtiBinarySemHandle == NULL) || (debugTaskBinarySemHandle == NULL))
	{
		return ERROR_STATUS;
	}

	return OK_STATUS;
}


/**
  * @brief  Create the thread(s)
  * @retval Status creating
  */
StatusFunctionExecution_t createThreads(void)
{
	/* creation of debounceTask */
	debounceTaskHandle = osThreadNew(StartDebounceTask, NULL, &debounceTask_attributes);

	/* creation of blinkingTask */
	blinkingTaskHandle = osThreadNew(StartBinkingTask, NULL, &blinkingTask_attributes);

	/* creation of debuggingTask */
	debuggingTaskHandle = osThreadNew(StartDebuggingTask, NULL, &debuggingTask_attributes);

	/* creation of watchdogTask */
	watchdogTaskHandle = osThreadNew(StartWatchdogTask, NULL, &watchdogTask_attributes);

	if ((debounceTaskHandle == NULL) || (blinkingTaskHandle == NULL) ||
		(debuggingTaskHandle == NULL) || (watchdogTaskHandle == NULL))
	{
		return ERROR_STATUS;
	}

	return OK_STATUS;
}

/**
  * @brief  Function implementing the debounceTask thread.
  * @param  argument: Not used
  * @retval None
  */
void StartDebounceTask(void *argument)
{
	StatusButton_t ButtonState;
	ButtonState = initializeButtonState();
    /* Infinite loop */
    for(;;)
    {
    	xSemaphoreTake(buttonExtiBinarySemHandle, portMAX_DELAY);
    	while (1)
    	{
    		StatusStateCheck_t TempStatusChecking = checkButtonStateChange(ButtonState);

			if (TempStatusChecking == CHANGED)
			{
				if (ButtonState == IDLE)
				{
				    ButtonState = CLICK;

			    	addLogMessageWithoutValueWrapped(&TxRingBuffer, "Button is clicked");
				    increasePwmLedPulse(&PmwLed);
			    }
			    else if (ButtonState == CLICK)
			    {
				    ButtonState = IDLE;
			    }

			    break;
			}
			else if (TempStatusChecking == NO_CHANGED)
			{
				break;
			}
    		osDelay(10);
    	}
    	/* Enable button interruptions */
		HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    }
}

/**
* @brief Function implementing the blinkingTask thread.
* @param argument: Not used
* @retval None
*/
void StartBinkingTask(void *argument)
{
  /* Infinite loop */
  for(;;)
  {
	  togglePwmLed(&PmwLed);
	  if (IS_LED_ON(PmwLed.State))
	  {
		  /* Update the timer for 200 ms */
		  osDelay(PERIOD_200_MS);
	  }
	  else if (IS_LED_OFF(PmwLed.State))
	  {
		  /* Update the timer for 100 ms */
		  osDelay(PERIOD_100_MS);
	  }
  }
}

/**
* @brief Function implementing the debuggingTask thread.
* @param argument: Not used
* @retval None
*/
void StartDebuggingTask(void *argument)
{
	StatusFunctionExecution_t PrintStatus;
  /* Infinite loop */
  for(;;)
  {
	  xSemaphoreTake(debugTaskBinarySemHandle, portMAX_DELAY);
	  PrintStatus = printWrapped(&TxRingBuffer);
	  if (PrintStatus != OK_STATUS)
	  {
		  if (PrintStatus == NOT_FINISHED)
		  {
			  addLogMessageWithoutValueWrapped(&TxRingBuffer, "DMA is busy");
		  }
		  else
		  {
			  vTaskSuspend(debuggingTaskHandle);
		  }
	  }

	  /* if there is still data in the ring buffer, give the semaphore again */
	  if (RB_getNumberOfItems(TxRingBuffer.RingBuffer))
	  {
		  xSemaphoreGive(debugTaskBinarySemHandle);
	  }

	  osDelay(DELAY_DEBUGGING_TASK);
  }
}

/**
* @brief Function implementing the watchdogTask thread.
* @param argument: Not used
* @retval None
*/
void StartWatchdogTask(void *argument)
{
  /* Infinite loop */
  for(;;)
  {
	  osDelay(DELAY_WATCHDOG_TASK);
	  IWDG_HandleTypeDef* iwdgHandle = getWatchdogHandle();
	  HAL_IWDG_Refresh(iwdgHandle);
  }
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM5 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == TIM5) {
    HAL_IncTick();
  }
}

#else
#endif /* MX_DEBUGGING */
