/**
  ******************************************************************************
  * @file    os_wrapped.c
  * @brief   FreeRTOS Wrapped Functions Module.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Nov 10, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "os_wrapped.h"

/* Private define ------------------------------------------------------------*/
#define TWO_CHARS_SIZE	  2
/* Private variables ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
extern osSemaphoreId_t debugTaskBinarySemHandle;
extern osMutexId_t Uart2MutexHandle;

/**
  * @brief Add new char items to Ring Buffer
  * @note  This function is wrapped RB_putNewItems() function with critical sections
  * @param RingBuffer: pointer to ring buffer structure
  * @param Item: pointer to new char items
  * @param Size: size of new char items
  * @retval Status function
  */
StatusRingBuffer_t RB_putNewItemsWrapped(RingBuffer_t* RingBuffer, const char* Item, uint8_t Size)
{

    if ((RingBuffer == NULL) || (Item == NULL))
    {
    	return RB_INVALID_PARAMS;
    }

    for (uint8_t i = 0; i < Size; i++)
    {
    	taskENTER_CRITICAL();
    	/* add new item */
        *(RingBuffer->InputItem) = *(Item + i);

        /* pointer shift */
        RingBuffer->InputItem++;

        /* check buffer overrun */
        if (RingBuffer->InputItem >= (RingBuffer->Head + RingBuffer->Length))
        {
        	RingBuffer->InputItem = RingBuffer->Head;
        }

        /* check overwriting of old items */
        if (RingBuffer->NumberOfItems < RingBuffer->Length)
        {
        	RingBuffer->NumberOfItems++;
        }
        taskEXIT_CRITICAL();
    }

    return RB_OK;
}

/**
  * @brief Addition log message to buffer
  * @param Buffer: pointer to common buffer structure
  * @param format: log message array
  * @param Value: Value log
  * @retval Status execution
  */
StatusFunctionExecution_t addLogMessageWrapped(CommonBuffer_t* Buffer,
										       const char* format, uint32_t Value)
{
	StatusFunctionExecution_t StatusAdding = addLogMessage(Buffer, format, Value);
	if (StatusAdding == OK_STATUS)
	{
		xSemaphoreGive(debugTaskBinarySemHandle);
		return OK_STATUS;
	}
	else
	{
		return ERROR_STATUS;
	}
}

/**
  * @brief Addition log message to buffer without value
  * @param Buffer: pointer to common buffer structure
  * @param format: log message array
  * @retval Status execution
  */
StatusFunctionExecution_t addLogMessageWithoutValueWrapped(CommonBuffer_t* Buffer,
													       const char* format)
{
	StatusFunctionExecution_t StatusAdding = addLogMessageWithoutValue(Buffer, format);
	if (StatusAdding == OK_STATUS)
	{
		xSemaphoreGive(debugTaskBinarySemHandle);
		return OK_STATUS;
	}
	else
	{
		return ERROR_STATUS;
	}
}

/**
  * @brief Print logged message
  * @param Buffer: pointer to common buffer structure
  * @retval Status execution
  */
StatusFunctionExecution_t printWrapped(CommonBuffer_t* Buffer)
{
	StatusFunctionExecution_t StatusPrint = OK_STATUS;

	xSemaphoreTake(Uart2MutexHandle, portMAX_DELAY);

	if (Buffer->BufferType == RING_BUFFER_TX)
	{
		uint16_t NumberOfItems = RB_getNumberOfItemsBeforeEnd(Buffer->RingBuffer);
		if (NumberOfItems == 0)
		{
			xSemaphoreGive(Uart2MutexHandle);
			StatusPrint = INVALID_PARAMS;
			return StatusPrint;
		}

		uint8_t* RingBufferOutputItem = RB_getOutputItemPointer(Buffer->RingBuffer);

		StatusUart_t UartStatus;
		UartStatus = UART_transmitDmaRing(RingBufferOutputItem, NumberOfItems);
		if (UartStatus == UART_STATE_OK)
		{
			StatusRingBuffer_t StatusDecrease = RB_decreaseNumberOfItems(Buffer->RingBuffer, NumberOfItems);
			if (StatusDecrease != RB_OK)
			{
				StatusPrint = ERROR_STATUS;
			}
			else
			{
				StatusPrint = OK_STATUS;
			}
		}
		else if (UartStatus == UART_STATE_ERROR)
		{
			StatusPrint = ERROR_STATUS;
		}
		else
		{
			StatusPrint = NOT_FINISHED;
		}
	}

	xSemaphoreGive(Uart2MutexHandle);

	return StatusPrint;
}


