/**
  ******************************************************************************
  * @file           : os_wrapped.h
  * @brief          : Header for os_wrapped.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 18, 2022
  *
  ******************************************************************************
  */

#ifndef SRC_OS_WRAPPED_H_
#define SRC_OS_WRAPPED_H_

/* Includes ------------------------------------------------------------------*/
#include "cmsis_os.h"
#include "semphr.h"

#include "common.h"
#include "logging.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusRingBuffer_t RB_putNewItemsWrapped(RingBuffer_t*, const char*, uint8_t);
StatusRingBuffer_t RB_decreaseNumberOfItemsWrapped(RingBuffer_t*, uint16_t);

StatusFunctionExecution_t printWrapped(CommonBuffer_t*);

StatusFunctionExecution_t addLogMessageWithoutValueWrapped(CommonBuffer_t*,
													       const char*);
StatusFunctionExecution_t addLogMessageWrapped(CommonBuffer_t*, const char*,
											   uint32_t);
/* Private defines -----------------------------------------------------------*/


#endif /* SRC_OS_WRAPPED_H_ */
